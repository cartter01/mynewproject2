$user = 'ck438311@ad.sprint.com' #add your ADID user
$email = 'carlos.pitter@sprint.com' #Add your sprint email
$pass = 'Apex_060623:' #Add your ADID password

$pair = "$($user):$($pass)"
$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic $encodedCreds"
$Headers = @{
 Authorization = $basicAuthValue
}

$LogsDate = (Get-Date).ToString('yy-MM-dd-hh-mm-ss')
$path = '~\Documents\Logs'
if(!(Test-Path -Path $path)){
    # file with path $path doesn't exist
    New-Item -Path $path -ItemType Directory
}
$Logfile = "~\Documents\Logs\jira-logs-$($LogsDate).log"

Function LogWrite
{
   Param ([string]$logstring)

   Add-content $Logfile -value $logstring
}

#Prepare for pulling List Info------------------------------------
Add-Type -Path 'C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SharePoint.Client\v4.0_16.0.0.0__71e9bce111e9429c\Microsoft.SharePoint.Client.dll'
$SiteURL = "https://tmobileusa.sharepoint.com/sites/TPAS"
$ListName="TPAS - Jira Tickets"
$Context = New-Object Microsoft.SharePoint.Client.ClientContext($SiteURL)
$securePassword=ConvertTo-SecureString $pass -AsPlainText -Force
$Context.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($email, $securePassword)
$Web = $Context.Web
$List = $web.get_lists().getByTitle($ListName)

$ListItems = $List.GetItems([Microsoft.SharePoint.Client.CamlQuery]::CreateAllItemsQuery())
$Context.Load($ListItems)
$Context.ExecuteQuery()

#Loop throught list for updating -------------------------------------------------------------------------------
function Get-Update {

    write-host ""
    write-host "******** Updating Current Tickets ********"
    write-host ""
    write-host "Getting info from Sharepoint List"
    write-host "Loading..."

    LogWrite ""
    LogWrite "******** Updating Current Tickets ********"
    LogWrite ""
    LogWrite "Getting info from Sharepoint List"
    LogWrite "Loading..."LogWrite


    $ListItems | ForEach-Object {
        #Get the Title field value
        write-host $_["Title"]
        LogWrite $_["Title"]
        
        #Filter by the opened ones only
        if (  ! ($_["Status"] -like "*closed*" -and $_["CloseCheck"] -eq "1") ){

            $response = (Invoke-WebRequest -Uri "https://jirasw.t-mobile.com/rest/api/2/search?jql=key=$($_["Title"])" -Headers $Headers).content

            $created = $response | jq -r '.issues[] | [.fields.created] | @tsv'
            $updated = $response | jq -r '.issues[] | [.fields.updated] | @tsv'
            $status = $response | jq -r '.issues[] | [.fields.status.name] | @tsv'
            $assigned = $response | jq -r '.issues[] | [.fields.components[].description] | @tsv'

            #create duration
            $predetermined=[system.datetime]($created)
            $current_date = (Get-Date).ToString('yyyy-MM-dd hh:mm:ss tt')
            $ts = New-TimeSpan -Start $predetermined -End $current_date
            $Updtduration = "$($ts.Days) days $($ts.Hours) hours"

            $predeterminedupdate=[system.datetime]($updated)
            $real_upt_dated=$predeterminedupdate.AddHours(+7).ToString('yyyy-MM-dd hh:mm:ss tt')

            if( $_["Status"] -like "Closed"){
                $PredetermineUpdt=[system.datetime]($updated)
                $tsi = New-TimeSpan -Start $predetermined -End $PredetermineUpdt
                $Updtduration = "$($tsi.Days) days $($tsi.Hours) hours"

                $CloseCheck = "1"
                write-host "Closed Ticket Updated"
                LogWrite "Closed Ticket Updated"
            }


            #Update item
            try{
                $ListItem = $List.GetItemById($_["ID"])

                $ListItem["UpdatedDate"] = $real_upt_dated
                $ListItem["Dayshoursopened"] = $Updtduration
                $ListItem["Status"] = $status
                $ListItem["Assignedteam"] = $assigned
                $ListItem["CloseCheck"] = $CloseCheck
                if( $_["Status"] -like "Closed"){
                    $ListItem["CloseCheck"] = "1"
                    
                }
                if( $_["CloseCheck"] -eq "1" -and $_["Status"] -notlike "Closed" ){
                    $ListItem["CloseCheck"] = "0"
                }

                $ListItem.Update()

                $Context.ExecuteQuery()
                write-host "Item Updated!" -foregroundcolor Green
                LogWrite "Item Updated!" -foregroundcolor Green
            }
            catch{
                write-host "$($_.Exception.Message)" -foregroundcolor red
                LogWrite "$($_.Exception.Message)" -foregroundcolor red
            }
        }
    }
    write-host "Update Completed!"
}

#Add New Tickets ------------------------------------------------------------------------------------------
function Get-AddNew {

    write-host ""
    write-host "******** Add New Tickets ********"
    write-host ""
    write-host "Getting info from Jira"
    write-host "Loading..."

    LogWrite ""
    LogWrite "******** Add New Tickets ********"
    LogWrite ""
    LogWrite "Getting info from Jira"
    LogWrite "Loading..."

    $response = (Invoke-WebRequest -Uri 'https://jirasw.t-mobile.com/rest/api/2/search?jql=labels+in+(TPAS,+tpas,+TPAS-Bugs,+TPAS-Bugs-Production)+AND+issuetype+=+Bug+AND+status+not+in+(Done,+Withdrawn,+Closed)&maxResults=500' -Headers $Headers).content

    $key = $response | jq -r '.issues[] | [.key] | @tsv'
    $id = $response | jq -r '.issues[] | [.id] | @tsv'
    $summary = $response | jq -r '.issues[] | [.fields.summary] | @tsv'
    $description = $response | jq -r '.issues[] | [.fields.description] | @tsv'
    $created = $response | jq -r '.issues[] | [.fields.created] | @tsv'
    $updated = $response | jq -r '.issues[] | [.fields.updated] | @tsv'
    $status = $response | jq -r '.issues[] | [.fields.status.name] | @tsv'
    $labels = $response | jq -r '.issues[] | [.fields.labels[]] | @tsv'
    $assigned = $response | jq -r '.issues[] | [.fields.components[].description] | @tsv'


    write-host "Comparing..."
    LogWrite "Comparing..."

    $addornot = (Get-Date).AddDays(-4).ToString('yyyy-MM-dd hh:mm:ss tt')

    for ( $index = 0; $index -lt $key.count; $index++){

        "Item: {0}" -f $key[$index]
        

        if ( $created[$index] -gt $addornot ){
            
            $count = 0

            
             
            #Loop through each item 
            #Check if item is already added
            $ListItems | ForEach-Object {
                if ( $id[$index]  -eq $_["TID"] ){
                    write-host "Item Already in List"

                    LogWrite "Item Already in List"
                    $count = 1
                }
            }

           #Add item if it is not already
            if ( $count -eq 0 ){

                write-host "Item $($key[$index]) not in List"
                write-host "Adding..."

                LogWrite "Item $($key[$index]) not in List"
                LogWrite "Adding..."

                #Get ticket environments
                $env = ""
                if ( $summary[$index] -like "*(PROD*" ){
                $env = "PROD"
                
                }
                else{

                    if ( $summary[$index] -like "*QLAB02*" -or $description[$index] -like "*QLAB02*" ) {
                        $env = "QLAB02"
                    }
                    if ( $summary[$index] -like "*QLAB03*" -or $description[$index] -like "*QLAB03*" ) {
                        if( $env ){$env = $( $env + " & " )}
                        $env = $( $env + "QLAB03" )
                    }
                    if ( $summary[$index] -like "*PIT01*" -or $description[$index] -like "*PIT01*" ) {
                        if( $env ){$env = $( $env + " & " )}
                        $env = $( $env + "PIT01" )
                    }
                    if ( $summary[$index] -like "*PIT02*" -or $description[$index] -like "*PIT02*" ) {
                        if( $env ){$env = $( $env + " & " )}
                        $env = $( $env + "PIT02" )
                    }
                    if ( $summary[$index] -like "*PLAB01*" -or $description[$index] -like "*PLAB01*" ) {
                        if( $env ){$env = $( $env + " & " )}
                        $env = $( $env + "PLAB01" )
                    }
                    if ( $summary[$index] -like "*PLAB02*" -or $description[$index] -like "*PLAB02*" ) {
                        if( $env ){$env = $( $env + " & " )}
                        $env = $( $env + "PLAB02" )
                    }

                    if ( [string]::IsNullOrEmpty($env) ){ 
                        
                        $env = "Other"
                    }
                }

                #Get if the ticket is opened by a partner
                $opened_by = "TPAS"

                if ( $labels[$index] -like "*TPAS-APPLE-Reported-Bugs*" ) {$opened_by = "APL"}
                elseif ( $labels[$index] -like "*TPAS-BBY-Reported-Bugs*" ) {$opened_by = "BBY"}
                elseif ( $labels[$index] -like "*TPAS-WirelessAdvocates-Reported-Bugs*" ) {$opened_by = "WIA"}
                elseif ( $labels[$index] -like "*TPAS-Walmart-Reported-Bugs*" ) {$opened_by = "WAL"}

                #create duration
                $predetermined=[system.datetime]($created[$index])
                $real_date = $predetermined.AddHours(+7).ToString('yyyy-MM-dd hh:mm:ss tt')
                $current_date = (Get-Date).ToString('yyyy-MM-dd hh:mm:ss tt')
                $ts = New-TimeSpan -Start $predetermined -End $current_date
                $duration = "$($ts.Days) days $($ts.Hours) hours"
                $created[$index]
                $predetermined
                $real_date

                $predeterminedupt=[system.datetime]($updated[$index])
                $real_upt_date=$predeterminedupt.AddHours(+7).ToString('yyyy-MM-dd hh:mm:ss tt')

                #Update Description
                $updt1 = $description[$index].replace('\n', ' ')
                $updt2 = $updt1.replace('\r', ' ')
                $updt3 = $updt2.replace('\', '')
                $updt4 = $updt3.replace('?', ' ')

                #Update and Push---------------------------------------------------------------
                try{
                    $itemCreateInfo = New-Object Microsoft.SharePoint.Client.ListItemCreationInformation
                    $listItem = $list.addItem($itemCreateInfo)
                    $listItem.set_item('TID', $id[$index])
                    $listItem.set_item('Title', $key[$index])
                    $listItem.set_item('Summary', $summary[$index])
                    $ListItem.set_item('Description', $updt4)
                    $ListItem.set_item('Environment', $env)
                    $ListItem.set_item('CreatedDate', $real_date)
                    $ListItem.set_item('UpdatedDate', $real_upt_date)
                    $ListItem.set_item('Dayshoursopened', $duration)
                    $ListItem.set_item('Status', $status[$index])
                    $ListItem.set_item('NRs', $opened_by)
                    $ListItem.set_item('Assignedteam', $assigned[$index])
                    $ListItem.set_item('Link', "https://jirasw.t-mobile.com/browse/$($key[$index])")


                    $listItem.update();
                    $Context.Load($listItem)
                    $Context.ExecuteQuery()

                    write-host "Item Updated!" -foregroundcolor Green
                    LogWrite "Item Updated!" -foregroundcolor Green
                }
                catch{
                    write-host "$($_.Exception.Message)" -foregroundcolor red
                    LogWrite "$($_.Exception.Message)" -foregroundcolor red
                }
            }

        } else{
            "Too old"
        }
    } 

    write-host "New Tickets has been added"
    LogWrite "New Tickets has been added"
}

function Get-Main {
    write-host ""
    write-host "******************************"
    write-host "*      Choose an Option      *"
    write-host "******************************"
    write-host ""
    write-host " 1 ----- Update & Add New"
    write-host " 2 ----- Update only"
    write-host " 3 ----- Add New only"
    write-host " 4 ----- Exit"
    write-host ""
    $option= Read-Host 'Number of your choice?'
    write-host ""

    switch ($option)
    {
        1 {"---> You Selected to Update & Add New"; LogWrite "---> You Selected to Update & Add New"; Get-Update; Get-AddNew; Break}
        2 {"---> You Selected to Update only"; LogWrite "---> You Selected to Update only"; Get-Update; Break}
        3 {"---> You Selected to Add New only"; LogWrite "---> You Selected to Update only"; Get-AddNew; Break}
        4 {exit; Break}
        Default {
            ""
            "Select one of the options!!!"
            Get-Main
        }
    }
}

Get-Main