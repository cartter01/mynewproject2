#!/bin/bash

user="ck438311@ad.sprint.com" #Replace with your Jira User
password="Apex_060623:" #Add your jira Password

# Environmental arrays
environment+=("qlab02-ext");
hst+=("stg-w2");
environment+=("qlab03-ext");
hst+=("stg-w2");
environment+=("plab01-ext");
hst+=("plb-w2");
environment+=("pit02-ext");
hst+=("stg-w2");
environment+=("prodtest-ext");
hst+=("cde-prd-w2");
environment+=("production-ext");
hst+=("cde-prd-w2");
apps+=("/v1/account");
apps+=("/v2/account");
apps+=("/v3/account");
apps+=("/v2/activate-order");
apps+=("/v1/address");
apps+=("/v1/agent-code");
apps+=("/v1/agreements/service");
apps+=("/v2/agreements");
apps+=("/v1/credit");
apps+=("/v1/customer");
apps+=("/v1/device");
apps+=("/v1/finance");
apps+=("/v1/format");
apps+=("/v1/fraud");
apps+=("/v1/imei-swap");
apps+=("/v1/network");
apps+=("/v1/order");
apps+=("/v2/order");
apps+=("/v1/partner");
apps+=("/v1/partner-profiles");
apps+=("/v1/payments");
apps+=("/v1/port-in");
apps+=("/v2/port-in");
apps+=("/v1/price");
apps+=("/v2/price");
apps+=("/v1/product");
apps+=("/v2/product");
apps+=("/v1/reference");
apps+=("/v1/simchange");
apps+=("/v1/trade-in");
apps+=("/v2/trade-in");
apps+=("/v1/utils");
cntl=0;
count=0;
time=$(date +'%m-%d-%Y.%H.%M');
conftime=$(date +'%m/%d/%Y %H:%M %p PT');
countenv=0;
slash=", ";

#getting and formatting envs for the title

for enva in ${!environment[@]}; do
   newenv[${countenv}]=${environment[$enva]} 
   countenv=$((${countenv} + 1));
done

newcountenv=$((${countenv} - 1));

formatenv=${newenv[*]/-ext/$slash};
rmslash=${formatenv%$slash};

baseenv=${newenv[0]/-ext/""};

echo ""
echo "****** This Script will Update the 'TPAS API Swagger Tracker' page ******"
echo ""
echo ""
echo "Getting the current API's data..."
echo ""

string="<p><strong>Swagger comparison and API Code CommitID deployed in ${rmslash} as of ${conftime}</strong></p><br /><table><colgroup><col /><col /><col /><col /></colgroup><tbody>";


# Iterate the APIs
for appi in ${!apps[@]}; do

   string="${string}<th style='text-align: left;'><br /></th><tr><th  colspan='4'>SERVICE: /tpas${apps[$appi]}</th></tr>";
   string="${string}<tr><th>Environment</th><th>swagger_version</th><th>Commit Id</th><th>Commit Time</th></tr>";

   for envi in ${!environment[@]}; do

        if [ "$count" == "0" ]; then
        echo "SERVICE: ${apps[$appi]}"
      fi
      cntl=$((${cntl} + 1));
      h='tpas-'${environment[$envi]}'.duck-'${hst[$envi]}'.kube.t-mobile.com';
      resource='/tpas'${apps[$appi]}'/actuator/info';
      response=$(curl -sk 'https://'$h$resource);
      info=$(jq '.' <<< $response);
      version=$(jq '.build.version' <<< $info );
      swagger_version=$(jq '.build.swagger_version' <<< $info );
      id=$(jq '.git.commit.id' <<< $info );
      # Check for blank ID
      if [ "$id" != "" ]; then
         # Format the fields
         t=$(jq '.git.commit.time' <<< $info );
         t=$(tr -d '"' <<< ${t:0:22});
         id=$(tr -d '"' <<< $id);
         version=$(tr -d '"' <<< $version);
         swagger_version=$(tr -d '"' <<< $swagger_version);
         if [ "$version" == "null" ]; then version="---"; fi;

        

        echo "   Data optained for ${environment[$envi]}";
        string="${string}<tr><td>${environment[$envi]}</td><td>$swagger_version</td><td>$id</td><td>${t// /}</td></tr>"

        if [ "$count" == "$newcountenv" ]; then

            printf "*****************************************\n";
            count=$((${count} - ${newcountenv}));
        else
         count=$((${count} + 1));
        fi
            
            
      else
        echo "   Data optained for ${environment[$envi]}";
        echo ""

        string="${string}<tr><td>${environment[$envi]}</td><td colspan='5'> *** No Information ***</td></tr>"

         if [ "$count" == "$newcountenv" ]; then

            printf "*****************************************\n";
            count=$((${count} - ${newcountenv}));
            else
         count=$((${count} + 1));
         fi
      fi
   done
done
string="${string}</tbody></table>"

echo ""
echo ""
echo "API's data has been ontained"
echo ""
echo "****** Upploading info in Confluence ******"
echo ""
echo "Getting Jira file version..."
echo ""
echo ""

jiraresponse=$(curl -s -u $user:$password -X GET "https://confluencesw.t-mobile.com/rest/api/content/148172810")
jirainfo=$(jq '.' <<< $jiraresponse);
jiraversion=$(jq '.version.number' <<< $jirainfo);

echo "Current Version is: ${jiraversion}"
echo ""
jiraversion=$((${jiraversion} + 1));

echo "New Version is:     ${jiraversion}"
echo ""
echo ""
echo "***** Updating Info ******"
echo ""

curl -o /dev/null -u $user:$password -X PUT -H "Content-Type: application/json" -d "{\"id\":\"148172810\",\"type\":\"page\",\"title\":\"TPAS API Swagger Tracker\",\"space\":{\"key\":\"PEL\"},\"body\":{\"storage\":{\"value\":\"${string}\",\"representation\":\"storage\"}},\"version\":{\"number\":${jiraversion}}}" https://confluencesw.t-mobile.com/rest/api/content/148172810

echo ""
echo "Swaggers Confluence File has been Updated Successfully"
echo ""

sleep 30s