#!/bin/bash
# Environmental arrays
#environment+=("qlab02-ext");
#hst+=("stg-w2");
#environment+=("qlab03-ext");
#hst+=("stg-w2");
#environment+=("plab01-ext");
#hst+=("plb-w2");
#environment+=("pit02-ext");
#hst+=("stg-w2");
#environment+=("prodtest-ext");
#hst+=("cde-prd-w2");
environment+=("production-ext");
hst+=("cde-prd-w2");
apps+=("/v1/account");
cntl=0;
count=0;
tempversion="0.1";
time=$(date +'%m-%d-%Y.%H.%M');
countenv=0;
slash=" /";

#getting and formatting envs for the title

for enva in ${!environment[@]}; do
   newenv[${countenv}]=${environment[$enva]} 
   countenv=$((${countenv} + 1));
done

newcountenv=$((${countenv} - 1));

formatenv=${newenv[*]/-ext/$slash};
rmslash=${formatenv%/};

baseenv=${newenv[0]/-ext/""};

node mynewproject2/ScriptFiles
path=$(pwd)/tpas-all-comparison.${time}.html;
printf "\n";
printf "File will be saved on: ${path}\n";
printf "\n";
printf 'The comparison is based on %s\n' "${baseenv^^}" ;
printf "\n"


#Style for the html document
{ 
   printf "<style>\n";
   printf " .stylish {\n";
   printf "    font-family: Arial, Helvetica, sans-serif;\n";
   printf "    border-collapse: collapse;\n";
   printf "    width: 1000px;\n";
   printf "    margin:  auto;\n";
   printf "    margin-top:  15px;\n";
   printf " }\n";
   printf "\n";

   printf " .stylish td, .stylish th {\n";
   printf "    border: 1px solid #ddd;\n";
   printf "    padding: 5px;\n";
   printf " }\n";
   printf "\n";

   printf " .stylish tr:nth-child(even){\n   background-color: #f2f2f2;\n }\n";
   printf "\n"

   printf ".stylish tr:hover {\nbackground-color: #ddd;\n}\n";
   printf "\n";

   printf "  .stylish th {\n";
   printf "     padding-top: 6px;\n";
   printf "     padding-bottom: 12px;\n";
   printf "     text-align: left;\n";
   printf "     background-color: #f4f5f7;\n";
   printf "     color: black;\n";
   printf "  }\n";
   printf "\n";

   printf "  .color{\n   color: red;\n  }\n";
   printf "\n";

   printf "   h1 {\n";
   printf "     margin-top: 15px;\n";
   printf "     font-family: Helvetica;\n";
   printf "     text-align: center;\n";
   printf "     color: black;\n";
   printf "   }\n";

   printf "#EnvTitle {\n";
   printf "  color: #004d6f;\n";
   printf "}\n";
   printf "\n";

   printf "</style>\n";
   printf "\n";

   printf "<title>Env Comparison</title>\n";
   printf '<h1>TPAS <b id="EnvTitle"> %s </b> Environments Comparison</h1>\n' "${rmslash^^}";
   printf "\n";
} > "${path}"


# Iterate the APIs
for appi in ${!apps[@]}; do
   { 
      printf "<table class='stylish'>\n"
      printf "<tr><th colspan=\"5\">SERVICE: /tpas%s</th></tr>\n" ${apps[$appi]};
      printf "<tr><th>Environment</th><th>Version</th><th>Swagger</th><th>Commit Id</th><th>Commit Time</th></tr>\n";
   } >> "${path}"
   for envi in ${!environment[@]}; do
      cntl=$((${cntl} + 1));
      h='tpas-'${environment[$envi]}'.duck-'${hst[$envi]}'.kube.t-mobile.com';
      resource='/tpas'${apps[$appi]}'/actuator/info';
      response=$(curl -sk 'https://'$h$resource);
      info=$(jq '.' <<< $response);
      version=$(jq '.build.version' <<< $info );
      swagger_version=$(jq '.build.swagger_version' <<< $info );
      id=$(jq '.git.commit.id' <<< $info );
      # Check for blank ID
      if [ "$id" != "" ]; then
         # Format the fields
         t=$(jq '.git.commit.time' <<< $info );
         t=$(tr -d '"' <<< ${t:0:22});
         id=$(tr -d '"' <<< $id);
         version=$(tr -d '"' <<< $version);
         swagger_version=$(tr -d '"' <<< $swagger_version);
         if [ "$version" == "null" ]; then version="---"; fi;

            if [ "$count" == "0" ]; then
                  tempversion=${version};
                  {  
                    printf "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n" ${environment[$envi]} $version $swagger_version $id ${t// /};
                  } >> "${path}"

                  count=$((${count} + 1));
               else
                  if [ "$tempversion" != "$version" ]; then
                     message[${cntl}]="Version is not equal for ${apps[$appi]} on ${environment[$envi]}";
                     printf "${message[${cntl}]}\n";

                     {  
                        printf "<tr class='color'><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n" ${environment[$envi]} $version $swagger_version $id ${t// /};
                     } >> "${path}"
                  else
                     message[${cntl}]="Versions are equal for ${apps[$appi]} on ${environment[$envi]}";
                     printf "${message[${cntl}]}\n";

                     {  
                        printf "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n" ${environment[$envi]} $version $swagger_version $id ${t// /};
                     } >> "${path}"

                  fi

                  if [ "$count" == "$newcountenv" ]; then
                     printf "***********************************************************\n";
                     count=$((${count} - ${newcountenv}));

                  else
                     count=$((${count} + 1));
                  fi
               fi
      else
         message[${cntl}]="*** No Information *** for ${apps[$appi]} on ${environment[$envi]}";
         printf "${message[${cntl}]}\n";

         {     
            printf "<tr><td>%s</td><td colspan=\"5\"> *** No Information ***</td></tr>\n" ${environment[$envi]};
         } >> "${path}"

         if [ "$count" == "$newcountenv" ]; then
            printf "***********************************************************\n";
            count=$((${count} - ${newcountenv}));

         else
            count=$((${count} + 1));
         fi
      fi
   done
    { 
      printf "</table>\n";
   } >> "${path}"
done

{  
   printf "\n";
   printf "\n";
   printf "\n";
} >> "${path}"

printf "\n";
printf "Check your Scripts folder for the file called: tpas-all-comparison.${time}.html"
printf "\n";
printf "\n";

sleep 30s
